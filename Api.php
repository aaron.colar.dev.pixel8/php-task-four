<?php

require_once('MysqliDb.php');

/**
 * Tells the browser to allow code from any origin to access
 */
header("Access-Control-Allow-Origin: *");


/**
 * Tells browsers whether to expose the response to the frontend JavaScript code
 * when the request's credentials mode (Request.credentials) is include
 */
header("Access-Control-Allow-Credentials: true");



/**
 * Specifies one or more methods allowed when accessing a resource in response to a preflight request
 */
header("Access-Control-Allow-Methods: POST, GET, PUT, DELETE");

/**
 * Used in response to a preflight request which includes the Access-Control-Request-Headers to
 * indicate which HTTP headers can be used during the actual request
 */
header("Access-Control-Allow-Headers: Content-Type");

class Api
{
    private  $db;

    private $database = "employee";
    private $table = "information";



    function __construct()
    {
        $this->db = new MysqliDb("localhost", "root", "", $this->database);
    }

    public function httpGet($payload = array())
    {

        $query = $this->db->get($this->table);
        if ($query) {
            echo json_encode(array(
                'method' => 'GET',
                'status' => 'success',
                'data' => $query,
            ));
        } else {
            echo json_encode(array(
                'method' => 'GET',
                'status' => 'failed',
                'data' => [],
                'message' => 'Failed to fetch'
            ));
        }
    }

    public function httpPost($payload)
    {

        $query = $this->db->insert($this->table, $payload);
        if ($query) {
            echo json_encode(array(
                'method' => 'POST',
                'status' => 'success',
                'data' => $payload,
            ));
        } else {
            echo json_encode(array(
                'method' => 'POST',
                'status' => 'failed',
                'data' => [],
                'message' => 'Failed to insert'
            ));
        }
    }

    public function httpPut($id, $payload)
    {
        $this->db->where('id', $id);
        $query = $this->db->update($this->table, $payload);
        if ($query) {
            echo json_encode(array(
                'method' => 'PUT',
                'status' => 'success',
                'data' => $payload,
            ));
        } else {
            echo json_encode(array(
                'method' => 'PUT',
                'status' => 'failed',
                'data' => [],
                'message' => 'Failed to update'
            ));
        }
    }

    public function httpDelete($id, $payload)
    {
        $selected_id = ['id' => is_string($id) ? explode(",", $id) : null];

        if (count($selected_id['id'])) {
            $this->db->where('id', $selected_id['id'], "IN");
        } else {
            $this->db->where('id', $id);
        }


        $query = $this->db->delete($this->table);

        if ($query) {
            echo json_encode(array(
                'method' => 'DELETE',
                'status' => 'success',
                'data' => $payload
            ));
        } else {
            echo json_encode(array(
                'method' => 'DELETE',
                'status' => 'failed',
                'data' => [],
                'message' => 'Failed to delete'
            ));
        }
    }
}

$request_method = $_SERVER['REQUEST_METHOD'];

if ($request_method === 'GET') {
    $received_data = $_GET;
} else {
    if ($request_method === 'PUT' || $request_method === 'DELETE') {
        $request_uri = $_SERVER['REQUEST_URI'];
        $ids = null;
        $exploded_request_uri = array_values(explode("/", $request_uri));
        $last_index = count($exploded_request_uri) - 1;
        $ids = $exploded_request_uri[$last_index];
    }
}

$received_data = json_decode(file_get_contents('php://input'), true);

$api = new Api;

switch ($request_method) {
    case 'GET':
        $api->httpGet($received_data);
        break;
    case 'PUT':
        $api->httpPut($ids, $received_data);
        break;
    case 'POST':
        $api->httpPost($received_data);
        break;
    case 'DELETE':
        $api->httpDelete($ids, $received_data);
        break;
}
